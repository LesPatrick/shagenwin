﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHAGenerator
{
    public class Rainbow
    {
        public string pass { get; set; }
        public string hash { get; set; }
        public int steps { get; set; }
    }
}
