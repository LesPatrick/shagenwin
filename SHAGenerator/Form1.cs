﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SHAGenerator
{
    public partial class Form1 : Form
    {
        string[] baseDict = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "q", "s", "t", "u", "v", "w", "x", "y", "z",
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "Q", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
        };

        volatile SortedList<String, String> hashList = new SortedList<String, String>();
        volatile SortedList<String, String> passList = new SortedList<String, String>();

        Collection<Thread> threads = new Collection<Thread>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 1; i++)
            {
                var newThread = new Thread(() => generateSHA1(i, baseDict.Clone() as String[]));
                threads.Add(newThread);
                newThread.Start();
            }
        }

        public void generateSHA1(int part, String[] dict)
        {
            var shaProv = new SHA1CryptoServiceProvider();
            var password = "";
            var counter = 0;
            var max = (part + 1) * 13 > dict.Length ? dict.Length : (part + 1) * 13;
            for (int i = 13 * part; i < max; i++)
                foreach (String letter2 in dict)
                    foreach (String letter3 in dict)
                        foreach (String letter4 in dict)
                            foreach (String letter5 in dict)
                            {
                                password = dict[i] + letter2 + letter3 + letter4 + letter5;
                                var hash = GetSHA1CSPHash(password, shaProv);

                                lock (this.hashList)
                                {
                                    if (hashList.ContainsValue(hash) == true)
                                        continue;
                                }

                                for (int j = 2; j < 6; j++)
                                {
                                    var pass = hash.Substring(0, 5).ToLower();
                                    lock (this.hashList)
                                    {
                                        if (hashList.ContainsKey(pass) == true)
                                        {
                                            hashList.Add(password, hash);
                                            break;
                                        }
                                    }

                                    hash = GetSHA1CSPHash(pass, shaProv);
                                    lock (this.hashList)
                                    {
                                        if (hashList.ContainsValue(hash) == true)
                                            break;
                                    }

                                    if (j == 5) {
                                        lock (this.hashList)
                                        {
                                            hashList.Add(password, hash);
                                        }
                                    }
                                }

                                if (counter++ % 10000 == 0)
                                {
                                    Console.WriteLine("Obliczono: " + (counter).ToString() + " słów");
                                    Console.WriteLine("aktualne słowo 5 znakowe: " + password + " hash: " + hash);
                                }

                            }
        }

        public string GetSHA1CSPHash(string plaintext, SHA1CryptoServiceProvider shaprov)
        {
            byte[] hasedvalue = shaprov.ComputeHash(Encoding.Default.GetBytes(plaintext));
            StringBuilder str = new StringBuilder();
            for (int counter = 0; counter < hasedvalue.Length; counter++)
            {
                str.Append(hasedvalue[counter].ToString("X1")); //X1 is just used as format specifier, you can use as your requirement.  
            }
            return str.ToString();
        }

        public void TestGen()
        {
            var threads = 4;
            var threadList = new List<Thread>();
            for (int i = 0; i < threads; i++)
            {
                threadList.Add(new Thread(() =>
                {
                    var shaProv = new SHA1CryptoServiceProvider();
                    var password = "aaaaa";
                    var hash = GetSHA1CSPHash(password, shaProv);

                    for (int j = 0; j < Math.Pow(10, 7) / threads; j++)
                    {
                        var pass = hash.Substring(0, 5).ToLower();

                        hash = GetSHA1CSPHash(pass, shaProv);
                    }
                }));
            }

            foreach (Thread t in threadList)
            {
                t.Start();
            }
            foreach (Thread t in threadList)
            {
                t.Join();
            }

        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach (Thread thread in threads)
            {
                thread.Abort();
            }

            startPhraseLabel.Text = hashList.First().Key;
            endPhraseLabel.Text = hashList.Last().Key;
        }

        private void findHashBtn_Click(object sender, EventArgs e)
        {
            var found = false;
            if (hashList.ContainsValue(hashBox.Text.ToUpper()))
            {
                var pass = hashList.Keys[hashList.IndexOfValue(hashBox.Text)];
                for (int i = 0; i < 4; i++)
                {
                    pass = GetSHA1CSPHash(pass, new SHA1CryptoServiceProvider()).ToLower().Substring(0, 5);
                }
                hashBox.Text = pass;
            }
            else
            {
                var hash = hashBox.Text;
                for (int i = 0; i < 5; i++)
                {
                    hash = GetSHA1CSPHash(hash.ToLower().Substring(0, 5), new SHA1CryptoServiceProvider());
                    if (hashList.ContainsValue(hash))
                    {
                        var pass = hashList.Keys[hashList.IndexOfValue(hash)];
                        for (int j = 0; i < 5; i++)
                        {
                            var interHash = GetSHA1CSPHash(pass, new SHA1CryptoServiceProvider());
                            if (String.Compare(interHash.ToLower(), hashBox.Text.ToLower(), true) == 0)
                            {
                                hashBox.Text = pass;
                                found = true;
                                break;
                            }
                            pass = interHash.ToLower().Substring(0, 5);
                        }
                        if (found)
                            break;
                    }
                }
            }
        }
    }
}
